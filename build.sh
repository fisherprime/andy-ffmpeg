#!/bin/bash


# Avoid major Fuck ups
# --------------------------------------------------
#  Array indices effectively start at 0 for bash|ksh
#+ All other shells index arrays at 1

if [[ -n $BASH_VERSION ]]; then
	shopt -s nullglob
	array_start=0
else
	array_start=1
fi

if [[ -n $ZSH_VERSION ]]; then
	setopt NULL_GLOB
fi
# --------------------------------------------------


# Return statuses
# --------------------------------------------------
export E_CHANGE_DIRECTORY_FAIL=12   # Failure in changing directory
export E_FCREATION_FAIL=4           # File creation fail
export E_FNOT_FOUND=5               # File not found
export E_GENERAL_FAILURE=1          # General failure
export E_INVALID_PARAMETERS=7       # Invalid parameter(s)/ choice
export E_NET_CONN_FAIL=8            # Failed to connect to network profile
export E_options_PARSE_FAIL=9       # Failure parsing command options
export SUCCESS_RET=0                # Successfull termination

export INTERRUPT_TERM=130           # Termination by SIGINT|SIGTERM (143)
# --------------------------------------------------


# Flags
# --------------------------------------------------
export USE_STANDALONE=-1 # (0|1) {use NDK|standalone toolcahain}
export BUILD_LIBRARY=-1 # (0|1) {build project|external librariea}
# --------------------------------------------------


# Modules to build
# --------------------------------------------------
# Enable stuff via their respective libraries
# Not the only choice though
## Passing
# freetype # Needed for drawtext filter
# libvpx # Enable Include libsoxr resampling
# libwebp # Enable WebP encoding
# libzmq # Enable message passing, wont compile with ffmpeg
# ogg
# opus # Enable Opus de/encoding
# vorbis # Enable Vorbis en/decoding
# x264 # Enable H.264 encoding
# zlib
# soxr # Enable to include libsoxr resampling
#
## Failing
# fribidi # Improves drawtext filter
# gnutls # Needed for https support, requires libgmp, configure fails
# libass # Needed for subtitles and ass filter, relies on fribidi
# libxml2 # Enable XML parsing
# opencore-amr # Enable AMR-NB & enable AMR-WB decoding, wrong install folder
# openjpeg # Enable JPEG 2000 de/encoding, many dependency libs
#
# Not sourced
# libmp3lame # Enable MP3 encoding
# libgmp # High precision math library, needed for rtmp(t)e support if openssl or librtmp is not used
# xvid # Enable Xvid encoding
# --------------------------------------------------

#  Option flags
#+ -N use NDK toolchain
#+ -S use standalone toolchain


# Variables
# --------------------------------------------------
# Don't touch the variables preceeded by "_"
project_name=ffmpeg
android_min_version=O

# Parent directosy as the repo root
if [[ -d $GIT_HOME/andy-ffmpeg ]]; then
	parent_dir="$GIT_HOME/andy-ffmpeg"
else
	parent_dir="$PWD"

	while [[ $(echo "$parent_dir" | sed -E 's/.*andy-ffmpeg//' | wc -w) -gt 0 ]]; do
		parent_dir="$(echo "$parent_dir" | sed -E 's/\/\w*$//')"
	done
fi

_build_dir=
_build_dest=
build_log="$parent_dir/build_log.log"

_project_src_dir="$parent_dir/$project_name"
external_libs_dir="$parent_dir/external-libs"
_ext_libs_build_dir="$external_libs_dir/build"
common_libs_build_dest="$external_libs_dir/build/merged"

#  Should a submodule fail to build in batch
#+ try building it separate
#+ Submodules are specified By folder name
preferred_build_order=(
	"zlib" "libzmq" "freetype" "libxml2" "libgmp"
	"openssl" "libwebp" "ogg" "opus" "vorbis"
	"x264" "libvpx" "soxr" "libmp3lame" "xvid"
	)
# preferred_build_order=(
# 	"opencore-amr"
# 	)
_build_libs=( )

for lib in "${preferred_build_order[@]}"; do
	[[ ! -d $external_libs_dir/$lib ]] && continue

	_build_libs+=( "$external_libs_dir/$lib" )
done

state_file="$parent_dir/repo.state"
update_interval=51 # nth execution instance to update the submodules

_api_version=
_toolchain_dir=

#  Known CPU architectures
#+ ( "armv7a-neon" "armv7a" "arm64v8" )
cpu_archs=( "arm64v8a" )
sys_archs=( "arm" "arm64" "x86" "x86_64" )
_arch=

# Direcory name for the standalone toolchains
standalone_arm_32=my-32-toolchain
standalone_arm_64=my-64-toolchain

# Directory to store the standalone toolchains
_standalone_dir_32="$parent_dir/$standalone_arm_32"
_standalone_dir_64="$parent_dir/$standalone_arm_64"

_module=
_sysroot=
_abi=
_target_host=

_abi_dets_file="$parent_dir/abi.state"

_enable_cross_compile=0

_fake_pk="$parent_dir/fake-pk"

FLAGS=( )
EXTRA_CFLAGS=( )
EXTRA_LDFLAGS=( )
# --------------------------------------------------


# Functions
# --------------------------------------------------
update_stuff() {
	_update_repos() {
		for dir in "$external_libs_dir"/*; do
			[[ ! -d $dir || $dir == "$_ext_libs_build_dir" ]] && continue

			cd "$dir" || exit

			git checkout master
			git stash
			git pull

			cd "$external_libs_dir" || exit
		done

		cd "$_project_src_dir" || exit

		git checkout master
		git stash
		git pull

		cd "$parent_dir" || exit
	}

	_update_submodules() {
		printf "[+] Updating stuff\n"

		git submodule init
		git submodule update
	}

	if [[ ! -f "$state_file" ]]; then
		printf "[+] Pulling external libs & %s\n" "$project_name"

		_update_submodules

		touch repo.state
	else
		read -r repo_state < "$state_file"

		if [[ "$repo_state" =~ ^[0-9]+$ ]]; then
			if [[ $(( repo_state % update_interval )) -eq 0 ]]; then
				printf "[+] Updating external libs & %s\n" "$project_name"

				_update_submodules
				_update_repos
			fi
		else
			printf "[!] Unknown value in %s, resetting counter\n" "$state_file"
			printf "0\n" > "$state_file"
		fi

		printf "%s\n" "$(( repo_state + 1 ))" > "$state_file"
	fi
} # End update_stuff

prepare_env() {
	module="$1"

	_set_api_version() {
		#  {Abbreviation} {API versions}
		#+ L	21
		#+ L1	22
		#+ M	23
		#+ N	24
		#+ N1	25
		#+ O	26
		#+ O1	27
		#+ P	28
		case "$android_min_version" in
			L)
				_api_version=21;;
			L1)
				_api_version=22;;
			M)
				_api_version=23;;
			N)
				_api_version=24;;
			N1)
				_api_version=25;;
			O)
				_api_version=26;;
			O1)
				_api_version=27;;
			P)
				_api_version=28;;
			*)
				printf "[!] I don't support that architecture\n"
				_api_version=-1
				;;
		esac
	}

	_export_env() {
		# If the $NDK environment variable isn't set
		if [[ -z $NDK ]]; then
			if [[ -n $ANDROID_NDK ]]; then
				printf "[+] NDK variable not set, assuming:
	%s\n" "$ANDROID_NDK"
				export NDK="$ANDROID_NDK"
			else
				printf "[+] NDK variable not set, assuming:
	%s\n" "${HOME}/android-ndk"
				export NDK=${HOME}/android-ndk
			fi
		fi

		# Add android NDK toolchains to the system path
		_toolchain_dir="$NDK/toolchains/"
		for bin_dir in "$_toolchain_dir"*/prebuilt/linux-x86_64/bin; do
			[[ -d $bin_dir && $PATH != *"$bin_dir"* ]] && PATH="${PATH}:$bin_dir"
		done

		[[ -z $MAKEFLAGS ]] && export MAKEFLAGS=( "-j$(nproc)" )
	}

	_cleanup_previous_job() {
		[[ ! -d "$_build_dir" ]] && exit 1

		printf "[+] Cleaning up previous job for [%s]\n" "$(basename "$module")"

		[[ -d "$_build_dest" ]] && rm -rf "${_build_dest:?}"
		mkdir -p "$_build_dest"
	}

	_make_toolchains() {
		# USes clang rather than GCC

		printf "[+] Preparing standalone toolchains\n"

		# 32-bit toolchain
		if [[ ! -d $_standalone_dir_32 ]]; then
			"$NDK/build/tools/make_standalone_toolchain.py" --arch "${sys_archs[$(( array_start + 0))]}" --api "$_api_version" --install-dir="$_standalone_dir_32"&
		fi

		# 64-bit toolchain
		if [[ ! -d $_standalone_dir_64 ]]; then
			"$NDK/build/tools/make_standalone_toolchain.py" --arch "${sys_archs[$(( array_start + 1))]}" --api "$_api_version" --install-dir="$_standalone_dir_64"&
		fi

		wait
	}

	_set_api_version
	[[ $_api_version -lt 0 ]] && exit 1

	_export_env

	_cleanup_previous_job "$module"&
	[[ ! -d $common_libs_build_dest ]] && mkdir -p "$common_libs_build_dest"

	[[ USE_STANDALONE -eq 1 ]] && _make_toolchains

	wait

	cd "$module" || exit 1
} # End prepare_env

set_config_opts() {
	_export_common_flags() {
		EXTRA_CFLAGS=( )
		EXTRA_LDFLAGS=( )

		# Don't use the "-pie" flag anywhere but in the LDFLAGS
		cflags=(
			# "-std=gnu11"
			"-fPIE"
			"-fPIC"
			"-I$common_libs_build_dest/$_abi/include"
			)
		cxxflags=(
			# "-std=gnu++17"
			"-fPIE"
			"-fPIC"
			"-I$common_libs_build_dest/$_abi/include"
			)
		ldflags=( "-pie" "-L$common_libs_build_dest/$_abi/lib" )

		CFLAGS="${cflags[*]}"
		CXXFLAGS="${cxxflags[*]}"
		LDFLAGS="${ldflags[*]}"
	}

	#  Most configure scripts have the "--host" option
	#+ they then lack the "--enable-cross-compile" option
	case "$_module" in
		ffmpeg)
			FLAGS+=(
				"--enable-libfreetype"
				"--enable-libopus"
				"--enable-libvorbis"
				"--enable-libvpx"
				"--enable-libwebp"
				"--enable-libx264"
				"--enable-zlib"
				"--enable-libsoxr"
				# "--enable-gmp"
				# "--enable-gnutls"
				# "--enable-libass"
				# "--enable-libfribidi"
				# "--enable-libmp3lame"
				# "--enable-libopencore_amrnb"
				# "--enable-libopencore_amrwb"
				# "--enable-libopenjpeg"
				# "--enable-libx265"
				# "--enable-libxml2"
				# "--enable-libxvid"
				# "--enable-libzmq"
				)
			FLAGS+=(
				"--ar=$AR"
				"--as=$AS"
				"--cc=$CC"
				"--cxx=$CXX"
				"--ld=$LD"
				"--pkg-config=$_fake_pk"
				"--strip=$STRIP"
				)
			# Create a library that is linked to (put inside) binaries
			FLAGS+=(
				"--disable-shared"
				"--enable-static"
				)
			#  There is an issue with libavformat/network.c
			#+ implicit declaration of function 'closesocket' is
			#+ invalid in C99
			FLAGS+=(
				"--disable-network"
				)
			#  Setting "--target-os" to android will cause configure
			#+ to fail on inclusion of external libs & compile failure:
			#+ static declaration of 'truncf' follows non-static declaration
			FLAGS+=(
				"--target-os=android"
				)
			#  Sort out the B0 variable issue
			#+ brought up by sysroot/usr/include/asm-generic/termbits.h
			#+ which is included by ioctl.h
			FLAGS+=(
				"--disable-linux-perf"
				)
			FLAGS+=(
				"--arch=$_arch"
				"--disable-doc"
				"--disable-ffplay"
				"--disable-ffprobe"
				"--disable-indevs"
				"--disable-outdevs"
				"--disable-symver"
				"--enable-avresample"
				"--enable-decoders"
				"--enable-demuxers"
				"--enable-encoders"
				"--enable-ffmpeg"
				"--enable-filters"
				"--enable-gpl"
				"--enable-hwaccels"
				"--enable-indev=lavfi"
				"--enable-muxers"
				"--enable-parsers"
				"--enable-pic"
				"--enable-protocols"
				"--enable-small"
				"--enable-version3"
				"--optflags=-O2"
				"--sysroot=$_sysroot"
				# "--enable-jni"
				)

			if [[ $_enable_cross_compile -eq 1 ]]; then
				FLAGS+=( "--enable-cross-compile" )
			else
				FLAGS+=( "--cross-prefix=$_target_host" )
			fi

			EXTRA_CFLAGS+=( "-std=gnu11" )
			#  Create a shared library
			#+ Necessary or linking external libs fails
			EXTRA_LDFLAGS+=( "-shared" )
			;;
		freetype|fribidi|libass|libwebp| \
		libxml2|ogg|opus|vorbis|libzmq)
			#  Mostly autogen.sh then configure
			#+ gnutls uses bootstrap

			FLAGS+=(
				"--disable-shared"
				"--enable-static"
				"--host=$_target_host"
				"--with-pic"
				"--with-sysroot=$_sysroot"
				)

			[[ $_module == "opus" ]] && FLAGS+=( "--disable-doc" )

			if [[ $_module == "vorbis" ]]; then
				FLAGS+=(
					# Prefix where libogg is installed
					"--with-ogg=$external_libs_dir/build/ogg/$_abi"
					# Alternatively specify the -I & -L directories
					# --with-ogg-libraries=""
					# --with-ogg-includes=""
					)
			elif [[ $_module == "libass" ]]; then
				FLAGS+=( "--disable-require-system-font-provider" )
			elif [[ $_module == "libxml2" ]]; then
				FLAGS+=( "--with-zlib" )
			elif [[ $_module == "gnutls" ]]; then
				FLAGS+=( "--disable-doc" )
			elif [[ $_module == "soxr" ]]; then
				FLAGS+=( "--without-libltdl" )
				export LIBS="-lavformat -lavcodec -lavutil -lz -lx264"
			elif [[ $_module == "libwebp" ]]; then
				FLAGS+=( "--enable-libwebpmux" )
			fi

			_export_common_flags
			;;
		opencore-amr)
			# autoreconf -i

			FLAGS+=( "--disable-shared" "--enable-static" )

			_export_common_flags
			;;
		openjpeg|soxr)
			# cmake

			_export_common_flags
			;;
		x264)
			FLAGS+=(
				"--disable-cli"
				"--disable-shared"
				"--enable-pic"
				"--enable-static"
				"--enable-strip"
				"--host=$_target_host"
				"--sysroot=$_sysroot"
				)

			[[ $_enable_cross_compile -eq 0 ]] && \
				FLAGS+=( "--cross-prefix=$_target_host" )
			;;
		zlib)
			FLAGS+=( "--static" )

			[[ $_arch == *"64"* ]] && FLAGS+=( "--64" )

			_export_common_flags
			;;
		libvpx)
			FLAGS+=(
				"--disable-docs"
				"--disable-shared"
				"--enable-static"
				"--enable-vp8"
				"--enable-vp9"
				)

			case $_abi in
				armeabi-v7a)
					FLAGS+=( "--target=armv7-android-gcc" )
					;;
				arm64-v8a)
					FLAGS+=( "--target=arm64-android-gcc" )
					;;
				*)
					exit 1
					;;
			esac

			_export_common_flags
			;;
		openssl)
			case $_abi in
				armeabi*)
					FLAGS+=( "android-arm" )
					;;
				arm64*)
					FLAGS+=( "android-arm64" )
					;;
				*)
					exit 1
					;;
			esac

			export AR="ar"
			export AS="gcc"
			export CC="gcc"
			export CXX="g++"
			export LD="ld"
			export STRIP="strip"

			_export_common_flags
			;;
		*)
			printf "[!] Unknown library\n"

			exit 1
			;;
	esac
} # End set_config_opts

set_compile_opts() {
	_export_compile_stuff() {
		if [[ $_arch == "${sys_archs[$(( array_start + 0))]}" ]]; then
			_target_host="arm-linux-androideabi"

			cross_compile_bin="$_standalone_dir_32/bin"

			_sysroot="$_standalone_dir_32/sysroot"
		elif [[ $_arch == "${sys_archs[$(( array_start + 1))]}" ]]; then
			_target_host="aarch64-linux-android"

			cross_compile_bin="$_standalone_dir_64/bin"

			_sysroot="$_standalone_dir_64/sysroot"
		fi

		# This gcc is an alias of clang
		if [[ $USE_STANDALONE -eq 0 ]]; then
			# Directory containing the system headers and libraries for your target
			_sysroot="$NDK/platforms/android-$_api_version/arch-$_arch"

			export AR="$_target_host-ar"
			export AS="$_target_host-gcc"
			export CC="$_target_host-gcc"
			export CXX="$_target_host-g++"
			export LD="$_target_host-ld"
			export STRIP="$_target_host-strip"

			_enable_cross_compile=0
		elif [[ $USE_STANDALONE -eq 1 ]]; then
			export AR="$cross_compile_bin/$_target_host-ar"
			export AS="$cross_compile_bin/$_target_host-clang"
			export CC="$cross_compile_bin/$_target_host-clang"
			export CXX="$cross_compile_bin/$_target_host-clang++"
			export LD="$cross_compile_bin/$_target_host-ld"
			export STRIP="$cross_compile_bin/$_target_host-strip"

			_enable_cross_compile=1
		fi

		export PKG_CONFIG="$_fake_pk"

		printf "%s\n" "$_abi" > "$_abi_dets_file"

		set_config_opts
	}

	local cpu_arch="$1"

	printf "[+] Setting up compile options for %s\n" "$cpu_arch"

	FLAGS=( )
	#  Don't change the 2 below extra_*flags
	#+ Don't use the "-pie" flag anywhere but in the LDFLAGS
	EXTRA_CFLAGS=( "-fPIE" "-fPIC" )
	EXTRA_LDFLAGS=( "-pie" )

	export CFLAGS=
	export CXXFLAGS=
	export LDFLAGS=

	case "$cpu_arch" in
		armv7a-neon)
			[[ $USE_STANDALONE -eq 0 ]] && EXTRA_CFLAGS+=( "-mfpu=neon" )

			EXTRA_CFLAGS+=( "-march=armv7-a" "-mfloat-abi=softfp"  )
			EXTRA_LDFLAGS+=( "-Wl,--fix-cortex-a8" )
			_abi="armeabi-v7a"
			_arch="${sys_archs[$(( array_start + 0))]}"

			_export_compile_stuff
			;;
		armv7a)
			EXTRA_CFLAGS+=( "-march=armv7-a" "-mfloat-abi=softfp" )
			EXTRA_LDFLAGS+=( )
			_abi="armeabi-v7a"
			_arch="${sys_archs[$(( array_start + 0))]}"

			_export_compile_stuff
			;;
		arm64v8a)
			[[ $USE_STANDALONE -eq 0 ]] && EXTRA_CFLAGS+=( "-mfpu=neon" )

			EXTRA_CFLAGS+=( "-march=armv8-a" )
			EXTRA_LDFLAGS+=( )
			_abi="arm64-v8a"
			_arch="${sys_archs[$(( array_start + 1))]}"

			_export_compile_stuff
			;;
		*)
			printf "[!] Unknown CPU architeture\n"
			exit 1
			;;
	esac
} # End set_compile_opts

build() {
	_configure_and_make() {
		_configure_module() {
			# libvpx uses "Configure" with a "capital c"
			conf_bin=$(\find . -maxdepth 1 -iname configure)

			printf "[+] Configure binary: %s\n\n" "$conf_bin"

			chmod +x "$conf_bin"

			if [[ ${#EXTRA_CFLAGS} -lt 1 || ${#EXTRA_LDFLAGS} -lt 1 ]]; then
				"$conf_bin" ${FLAGS[*]} | tee "$_build_dest/configuration.txt"
			else
				"$conf_bin" ${FLAGS[*]} \
					--extra-cflags="${EXTRA_CFLAGS[*]}" \
					--extra-ldflags="${EXTRA_LDFLAGS[*]}" \
					| tee "$_build_dest/configuration.txt"
			fi

			# exit
		}

		printf "[+] Building for %s architecture\n" "$cpu_arch"

		set_compile_opts "$cpu_arch"

		if [[ ${#EXTRA_CFLAGS} -gt 0 || ${#EXTRA_LDFLAGS} -gt 0 ]]; then
			EXTRA_CFLAGS+=( "-I$common_libs_build_dest/$_abi/include" )
			EXTRA_LDFLAGS+=( "-L$common_libs_build_dest/$_abi/lib" )
		fi

		_build_dest="$_build_dest/$_abi"
		FLAGS+=( "--prefix=$_build_dest" )
		mkdir -p "$_build_dest"

		print_env | tee "$_build_dest/info.txt"

		if [[ -f bootstrap ]]; then
			printf "[+] Running bootstrap\n"

			chmod +x ./bootstrap
			./bootstrap --bootstrap-sync

			_configure_module
		elif [[ -f autogen.sh ]]; then
			printf "[+] Running autogen.sh\n"

			chmod +x ./autogen.sh

			_configure_module
		elif [[ -f configure || -f Configure ]]; then
			if [[ -f configure.ac ]]; then
				printf "[+] Running autoreconf\n"

				autoreconf --install
			fi

			_configure_module
		elif [[ -f CMakeLists.txt ]]; then
			printf "[+] Running cmake\n"

			# While specifying the build and source directories
			cmake -B"$_build_dest" \
			-H. \
			-DBUILD_SHARED_LIBS:BOOL=OFF \
			-DCMAKE_ANDROID_API:STRING="$_api_version" \
			-DCMAKE_ANDROID_ARCH:STRING="$_arch" \
			-DCMAKE_ANDROID_ARCH_ABI:STRING="$_abi" \
			-DCMAKE_ANDROID_NDK:PATH="$NDK" \
			-DCMAKE_EXE_LINKER_FLAGS:STRING="${ldflags[*]}" \
			-DCMAKE_INSTALL_PREFIX:PATH="$_build_dest" \
			-DCMAKE_SYSROOT:PATH="$_sysroot" \
			-DCMAKE_SYSTEM_NAME:STRING="Android" \
			| tee "$_build_dest/configuration.txt"

			# -DCMAKE_AR:PATH="$AR"
			# CMAKE_BINARY_DIR:PATH="$_build_dest"
			# CMAKE_HOME_DIRECTORY:PATH=.

			# Cmake variable typed
			# BOOL          = Boolean ON/OFF value.
			# PATH          = Path to a directory.
			# FILEPATH      = Path to a file.
			# STRING        = Generic string value.
			# INTERNAL      = Do not present in GUI at all.
			# STATIC        = Value managed by CMake, do not change.
			# UNINITIALIZED = Type not yet specified.

			cd $_build_dest || exit 1
		elif [[ -f build.ninja ]]; then
			printf "[+] Running ninja\n"

			ninja . | tee "$_build_dest/configuration.txt"
		elif [[ -f configure.ac ]]; then
			printf "[+] Running autoreconf\n"

			autoreconf --install
			#  Alternatively
			#+ autoconf; automake

			_configure_module
		fi

		[[ ${PIPESTATUS[1]} -eq 0 ]] || exit 1

		make -d clean
		make -d "${MAKEFLAGS[*]}" | tee "$_build_dest/make_log.txt" \
		|| return 1
		make install || return 1

		\cp -r "$_build_dest" "$common_libs_build_dest"
		\rm "$common_libs_build_dest/$_abi/"*.txt
	}

	printf "[*] Running build script\n"

	update_stuff

	# Default to the NDK toolchain
	[[ $USE_STANDALONE -eq -1 ]] && USE_STANDALONE=0

	if [[ $BUILD_LIBRARY -eq 1 ]]; then
		local build_count=0

		if [[ ! -d $external_libs_dir ]]; then
			printf "[!] Missing external libraries folder\n"
			exit 1
		fi

		if [[ $(\find "$external_libs_dir" | wc -l ) -le 0 ]]; then
			printf "[!] No external libraries in the folder\n"
			exit 1
		fi

		cd "$external_libs_dir" || exit 1
		_build_dir="$_ext_libs_build_dir"

		for ext_lib in "${_build_libs[@]}"; do
			_build_dest="$_build_dir/$(basename "$ext_lib")"

			prepare_env "$ext_lib"

			for cpu_arch in "${cpu_archs[@]}" ; do
				_module="$(basename "$ext_lib")"
				_configure_and_make
			done

			build_count=$(( build_count + 1 ))
		done

		printf "[+] Worked on %d submodules\n" "$build_count"
	else
		_build_dir="$parent_dir/build"
		_build_dest="$_build_dir/$project_name"

		prepare_env "$_project_src_dir"

		for cpu_arch in "${cpu_archs[@]}" ; do
			_module="$project_name"
			_configure_and_make
		done
	fi
} # End build

print_env() {
	printf "[+] Env:
	PWD: %s

	CC: %s
	CXX: %s

	FLAGS: %s" "$PWD" "$CC" "$CXX" "${FLAGS[*]}"

	if [[ ${#EXTRA_CFLAGS} -lt 1 || ${#EXTRA_LDFLAGS} -lt 1 ]]; then
		printf "
	CFLAGS: %s
	LDFLAGS: %s" "${CFLAGS[*]}" "${LDFLAGS[*]}"
	else
		printf "
	CFLAGS: %s
	LDFLAGS: %s" "${EXTRA_CFLAGS[*]}" "${EXTRA_LDFLAGS[*]}"
	fi

	printf "
	LIBS: %s

	Libs to build: %s

" "$LIBS" "${_build_libs[*]}"
} # End print_env

cleanup() {
	unset NDK
	unset AR
	unset AS
	# export CC
	# export CXX
	unset LD
	unset STRIP
} # End cleanup

main() {
	local command_name="./build.sh"
	_usage() {

	printf "[+] Usage: %s [options]

Options:
	-h,--help:        Print this help message
	-E,--ext-lib:     Compile external libraries
	-N,--ndk:         Use Android NDK toolchain
	-S,--standalone:  Use standalone toolchain
" "$command_name"

	} # End _usage

	if [[ $# -lt 1 ]]; then
		_usage
		logger "$command_name exit with status \"$E_INVALID_PARAMETERS\""
		return $E_INVALID_PARAMETERS
	fi

	local options
	options=$(getopt -o hENS -l "help","ext-lib","ndk","standalone" -n "$command_name" -- "$@")

	if [[ $? -ne 0 ]]; then
		printf '[!] Failed parsing command options\n'
		_usage
		return $E_options_PARSE_FAIL
	fi

	eval set -- "$options" # Don't know what this does

	while true; do
		case "$1" in
			-h|--help)
				_usage
				logger "$command_name exit with status \"$SUCCESS_RET\""
				return $SUCCESS_RET
				;;
			-E|--ext-lib)
				BUILD_LIBRARY=1
				;;
			-N|--ndk)
				if [[ USE_STANDALONE -ne -1 ]]; then
					printf '[!] Ambiguous toolchain options\n'
					_usage
					logger "$command_name exit with status \"$E_INVALID_PARAMETERS\""
					return $E_INVALID_PARAMETERS
				fi
				USE_STANDALONE=0
				;;
			-S|--standalone)
				if [[ USE_STANDALONE -ne -1 ]]; then
					printf '[!] Ambiguous toolchain options\n'
					_usage
					logger "$command_name exit with status \"$E_INVALID_PARAMETERS\""
					return $E_INVALID_PARAMETERS
				fi
				USE_STANDALONE=1
				;;
			--)
				break
				;;
			*)
				_usage
				logger "$command_name exit with status \"$E_INVALID_PARAMETERS\""
				return $E_INVALID_PARAMETERS
				;;
			esac
		shift # Shift positional parameters
	done

	# git subodule update

	build | tee "$build_log"
	cleanup
} # End main

main "$@"
# --------------------------------------------------


# Sources
# --------------------------------------------------
# ffmpeg https://git.ffmpeg.org/ffmpeg.git
# freetype git clone git://git.sv.nongnu.org/freetype/freetype2.git (https://www.freetype.org)
#! fribidi https://github.com/fribidi/fribidi
#! gnutls https://gitlab.com/gnutls/gnutls
#! libass https://github.com/libass/libass
# libwebp https://chromium.googlesource.com/webm/libwebp
# libxml2 https://gitlab.gnome.org/GNOME/libxml2
# libzmq https://github.com/zeromq/libzmq
# ogg https://github.com/xiph/ogg (https://xiph.org)
# opencore-amr git clone https://git.code.sf.net/p/opencore-amr/code (https://sourceforge.net/projects/opencore-amr/)
# openjpeg https://github.com/uclouvain/openjpeg
# opus https://github.com/xiph/opus (https://xiph.org)
# vorbis https://github.com/xiph/vorbis (https://xiph.org)
# x264 http://git.videolan.org/git/x264.git
# zlib https://github.com/madler/zlib # Will configure first few times then fail later on
#! lame https://svn.code.sf.net/p/lame/svn/trunk/lame lame-svn (https://sourceforge.net/projects/lame/)
#! x265 http://hg.videolan.org/x265
#! xvid https://www.xvid.com/
# libvpx https://chromium.googlesource.com/webm/libvpx
# soxr https://git.code.sf.net/p/soxr/code
# openssl https://github.com/openssl/openssl
# --------------------------------------------------
